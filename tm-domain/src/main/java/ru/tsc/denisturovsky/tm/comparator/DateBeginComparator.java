package ru.tsc.denisturovsky.tm.comparator;

import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.model.IHasDateBegin;

import java.util.Comparator;

public enum DateBeginComparator implements Comparator<IHasDateBegin> {

    INSTANCE;

    @Override
    public int compare(
            @Nullable final IHasDateBegin o1,
            @Nullable final IHasDateBegin o2
    ) {

        if (o1 == null || o2 == null) return 0;
        return o1.getDateBegin().compareTo(o2.getDateBegin());
    }

}
