package ru.tsc.denisturovsky.tm.exception.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.exception.AbstractException;

@NoArgsConstructor
public class AbstractUserException extends AbstractException {

    public AbstractUserException(@NotNull String message) {

        super(message);
    }

    public AbstractUserException(
            @NotNull String message,
            @NotNull Throwable cause
    ) {

        super(message, cause);
    }

    public AbstractUserException(@NotNull Throwable cause) {

        super(cause);
    }

    public AbstractUserException(
            @NotNull String message,
            @NotNull Throwable cause,
            boolean enableSuppression,
            boolean writableStackTrace
    ) {

        super(message, cause, enableSuppression, writableStackTrace);
    }

}
