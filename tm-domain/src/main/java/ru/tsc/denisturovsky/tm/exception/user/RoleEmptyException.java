package ru.tsc.denisturovsky.tm.exception.user;

public final class RoleEmptyException extends AbstractUserException {

    public RoleEmptyException() {

        super("Error! Role is empty.");
    }

}
