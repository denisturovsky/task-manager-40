package ru.tsc.denisturovsky.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.model.Project;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectListResponse extends AbstractResponse {

    @Nullable
    private List<Project> projects;

    public ProjectListResponse(@Nullable List<Project> projects) {

        this.projects = projects;
    }

}
