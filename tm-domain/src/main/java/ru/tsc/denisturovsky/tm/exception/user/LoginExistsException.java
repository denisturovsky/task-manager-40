package ru.tsc.denisturovsky.tm.exception.user;

public final class LoginExistsException extends AbstractUserException {

    public LoginExistsException() {

        super("Error! Login already exists...");
    }

}
