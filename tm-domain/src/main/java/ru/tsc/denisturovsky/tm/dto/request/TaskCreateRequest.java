package ru.tsc.denisturovsky.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TaskCreateRequest extends AbstractUserRequest {

    @Nullable
    private Date dateBegin;

    @Nullable
    private Date dateEnd;

    @Nullable
    private String description;

    @Nullable
    private String name;

    public TaskCreateRequest(@Nullable String token) {

        super(token);
    }

}
