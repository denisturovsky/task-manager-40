package ru.tsc.denisturovsky.tm.exception.field;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.exception.AbstractException;

@NoArgsConstructor
public class AbstractFieldException extends AbstractException {

    public AbstractFieldException(@NotNull String message) {

        super(message);
    }

    public AbstractFieldException(
            @NotNull String message,
            @NotNull Throwable cause
    ) {

        super(message, cause);
    }

    public AbstractFieldException(@NotNull Throwable cause) {

        super(cause);
    }

    public AbstractFieldException(
            @NotNull String message,
            @NotNull Throwable cause,
            boolean enableSuppression,
            boolean writableStackTrace
    ) {

        super(message, cause, enableSuppression, writableStackTrace);
    }

}
