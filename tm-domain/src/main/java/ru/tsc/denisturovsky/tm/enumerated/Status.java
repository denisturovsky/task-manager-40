package ru.tsc.denisturovsky.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.exception.field.StatusEmptyException;
import ru.tsc.denisturovsky.tm.exception.field.StatusIncorrectException;

import java.util.Arrays;
import java.util.Optional;

@Getter
public enum Status {

    NOT_STARTED("Not Started"),
    IN_PROGRESS("In Progress"),
    COMPLETED("Completed");

    @NotNull
    private final String displayName;

    Status(@NotNull String displayName) {

        this.displayName = displayName;
    }

    @NotNull
    public static String toName(final Status status) {

        if (status == null) return "";
        return status.getDisplayName();
    }

    @NotNull
    public static Status toStatus(@NotNull final String value) {

        if (value.isEmpty()) throw new StatusEmptyException();
        @Nullable final Optional<Status> statusOptional = Arrays.stream(values()).filter(m -> value.equals(m.name())).findFirst();
        if (statusOptional.orElse(null) == null) throw new StatusIncorrectException();
        return statusOptional.orElse(null);
    }

}
