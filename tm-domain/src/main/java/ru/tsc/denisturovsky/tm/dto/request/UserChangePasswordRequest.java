package ru.tsc.denisturovsky.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class UserChangePasswordRequest extends AbstractUserRequest {

    @Nullable
    private String newPassword;

    public UserChangePasswordRequest(@Nullable String token) {

        super(token);
    }

}
