package ru.tsc.denisturovsky.tm.exception.field;

public class IdEmptyException extends AbstractFieldException {

    public IdEmptyException() {

        super("Error! Id is empty...");
    }

}
