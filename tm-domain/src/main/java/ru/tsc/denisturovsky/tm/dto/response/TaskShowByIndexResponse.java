package ru.tsc.denisturovsky.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public final class TaskShowByIndexResponse extends AbstractTaskResponse {

    public TaskShowByIndexResponse(@Nullable final Task task) {

        super(task);
    }

}
