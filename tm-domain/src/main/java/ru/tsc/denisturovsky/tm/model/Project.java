package ru.tsc.denisturovsky.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.model.IWBS;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.util.DateUtil;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractUserOwnedModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    private Date created = new Date();

    @Nullable
    private Date dateBegin;

    @Nullable
    private Date dateEnd;

    @NotNull
    private String description = "";

    @NotNull
    private String name = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    public Project(
            @NotNull final String name,
            @NotNull final Status status,
            @NotNull final Date dateBegin
    ) {

        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    public Project(
            @NotNull final String name,
            @NotNull final Status status,
            @NotNull final Date dateBegin,
            @NotNull final Date dateEnd
    ) {

        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }

    @NotNull
    @Override
    public String toString() {

        return String.format(
                "%30s:%30s:%30s:%30s:%30s|",
                name,
                getStatus().getDisplayName(),
                description,
                DateUtil.toString(getDateBegin()),
                DateUtil.toString(getDateEnd())
        );
    }

}
