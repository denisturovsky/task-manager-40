package ru.tsc.denisturovsky.tm.exception.system;

import org.jetbrains.annotations.Nullable;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {

        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(@Nullable final String argument) {

        super("Error! Command ``" + argument + "`` not supported...");
    }

}
