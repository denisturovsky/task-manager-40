package ru.tsc.denisturovsky.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.denisturovsky.tm.api.service.*;
import ru.tsc.denisturovsky.tm.exception.field.IdEmptyException;
import ru.tsc.denisturovsky.tm.exception.field.UserIdEmptyException;
import ru.tsc.denisturovsky.tm.marker.UnitCategory;
import ru.tsc.denisturovsky.tm.model.Session;
import ru.tsc.denisturovsky.tm.model.User;
import ru.tsc.denisturovsky.tm.sevice.*;

import java.util.List;

import static ru.tsc.denisturovsky.tm.constant.SessionTestData.*;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class SessionServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private static final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private static final IUserService userService = new UserService(connectionService, projectService, taskService, propertyService);

    @NotNull
    private static String userId = "";

    @NotNull
    private final ISessionService service = new SessionService(connectionService);

    @BeforeClass
    public static void setUp() throws Exception {

        @NotNull final User user = userService.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        userId = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {

        @Nullable final User user = userService.findByLogin(USER_TEST_LOGIN);
        if (user != null) userService.remove(user);
    }

    @Test
    public void addByUserId() throws Exception {

        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(null, USER_SESSION3));
        Assert.assertNotNull(service.add(userId, USER_SESSION3));
        @Nullable final Session session = service.findOneById(userId, USER_SESSION3.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION3.getId(), session.getId());
    }

    @After
    public void after() throws Exception {

        service.clear(userId);
    }

    @Before
    public void before() throws Exception {

        service.add(userId, USER_SESSION1);
        service.add(userId, USER_SESSION2);
    }

    @Test
    public void clearByUserId() throws Exception {

        Assert.assertThrows(UserIdEmptyException.class, () -> service.clear(""));
        service.clear(userId);
        Assert.assertEquals(0, service.getSize(userId));
    }

    @Test
    public void existsByIdByUserId() throws Exception {

        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById("", NON_EXISTING_SESSION_ID));
        Assert.assertFalse(service.existsById(userId, ""));
        Assert.assertFalse(service.existsById(userId, NON_EXISTING_SESSION_ID));
        Assert.assertTrue(service.existsById(userId, USER_SESSION1.getId()));
    }

    @Test
    public void findAllByUserId() throws Exception {

        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(""));
        final List<Session> sessions = service.findAll(userId);
        Assert.assertNotNull(sessions);
        Assert.assertEquals(2, sessions.size());
        sessions.forEach(session -> Assert.assertEquals(userId, session.getUserId()));
    }

    @Test
    public void findOneByIdByUserId() throws Exception {

        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userId, ""));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById("", USER_SESSION1.getId()));
        Assert.assertNull(service.findOneById(userId, NON_EXISTING_SESSION_ID));
        @Nullable final Session session = service.findOneById(userId, USER_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1.getId(), session.getId());
    }

    @Test
    public void getSizeByUserId() throws Exception {

        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(""));
        Assert.assertEquals(2, service.getSize(userId));
    }

    @Test
    public void removeByIdByUserId() throws Exception {

        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeOneById(null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeOneById("", null));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeOneById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeOneById(userId, ""));
        service.removeOneById(userId, USER_SESSION2.getId());
        Assert.assertNull(service.findOneById(userId, USER_SESSION2.getId()));
    }

    @Test
    public void removeByUserId() throws Exception {

        service.remove(userId, USER_SESSION2);
        Assert.assertNull(service.findOneById(userId, USER_SESSION2.getId()));
    }

}