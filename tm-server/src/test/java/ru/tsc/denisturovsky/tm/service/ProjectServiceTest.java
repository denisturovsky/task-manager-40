package ru.tsc.denisturovsky.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.denisturovsky.tm.api.service.*;
import ru.tsc.denisturovsky.tm.comparator.NameComparator;
import ru.tsc.denisturovsky.tm.enumerated.Sort;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.denisturovsky.tm.exception.field.*;
import ru.tsc.denisturovsky.tm.marker.UnitCategory;
import ru.tsc.denisturovsky.tm.model.Project;
import ru.tsc.denisturovsky.tm.model.User;
import ru.tsc.denisturovsky.tm.sevice.*;

import java.util.Comparator;
import java.util.List;

import static ru.tsc.denisturovsky.tm.constant.ProjectTestData.*;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectService service = new ProjectService(connectionService);

    @NotNull
    private static final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private static final IUserService userService = new UserService(connectionService, service, taskService, propertyService);

    @NotNull
    private static String userId = "";

    @BeforeClass
    public static void setUp() throws Exception {

        @NotNull final User user = userService.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        userId = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {

        @Nullable final User user = userService.findByLogin(USER_TEST_LOGIN);
        if (user != null) userService.remove(user);
    }

    @Test
    public void addByUserId() throws Exception {

        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(null, USER_PROJECT3));
        Assert.assertNotNull(service.add(userId, USER_PROJECT3));
        @Nullable final Project project = service.findOneById(userId, USER_PROJECT3.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT3.getId(), project.getId());
    }

    @After
    public void after() throws Exception {

        service.clear(userId);
    }

    @Before
    public void before() throws Exception {

        service.add(userId, USER_PROJECT1);
        service.add(userId, USER_PROJECT2);
    }

    @Test
    public void changeProjectStatusById() throws Exception {

        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeProjectStatusById(null, USER_PROJECT1.getId(), status));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeProjectStatusById("", USER_PROJECT1.getId(), status));
        Assert.assertThrows(IdEmptyException.class, () -> service.changeProjectStatusById(userId, null, status));
        Assert.assertThrows(IdEmptyException.class, () -> service.changeProjectStatusById(userId, "", status));
        Assert.assertThrows(StatusEmptyException.class, () -> service.changeProjectStatusById(userId, USER_PROJECT1.getId(), null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.changeProjectStatusById(userId, NON_EXISTING_PROJECT_ID, status));
        service.changeProjectStatusById(userId, USER_PROJECT1.getId(), status);
        @Nullable final Project project = service.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(status, project.getStatus());
    }

    @Test
    public void clearByUserId() throws Exception {

        Assert.assertThrows(UserIdEmptyException.class, () -> service.clear(""));
        service.clear(userId);
        Assert.assertEquals(0, service.getSize(userId));
    }

    @Test
    public void create() throws Exception {

        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(null, USER_PROJECT3.getName()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create("", USER_PROJECT3.getName()));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, null));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, ""));
        @NotNull final Project project = service.create(userId, USER_PROJECT3.getName());
        Assert.assertNotNull(project);
        @Nullable final Project findProject = service.findOneById(userId, project.getId());
        Assert.assertNotNull(findProject);
        Assert.assertEquals(project.getId(), findProject.getId());
        Assert.assertEquals(USER_PROJECT3.getName(), project.getName());
        Assert.assertEquals(userId, project.getUserId());
    }

    @Test
    public void createWithDescription() throws Exception {

        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(null, USER_PROJECT3.getName(), USER_PROJECT3.getDescription()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create("", USER_PROJECT3.getName(), USER_PROJECT3.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, null, USER_PROJECT3.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, "", USER_PROJECT3.getDescription()));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(userId, USER_PROJECT3.getName(), null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(userId, USER_PROJECT3.getName(), ""));
        @NotNull final Project project = service.create(userId, USER_PROJECT3.getName(), USER_PROJECT3.getDescription());
        Assert.assertNotNull(project);
        @Nullable final Project findProject = service.findOneById(userId, project.getId());
        Assert.assertNotNull(findProject);
        Assert.assertEquals(project.getId(), findProject.getId());
        Assert.assertEquals(USER_PROJECT3.getName(), project.getName());
        Assert.assertEquals(USER_PROJECT3.getDescription(), project.getDescription());
        Assert.assertEquals(userId, project.getUserId());
    }

    @Test
    public void existsByIdByUserId() throws Exception {

        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById(null, NON_EXISTING_PROJECT_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById("", NON_EXISTING_PROJECT_ID));
        Assert.assertFalse(service.existsById(userId, ""));
        Assert.assertFalse(service.existsById(userId, NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(service.existsById(userId, USER_PROJECT1.getId()));
    }

    @Test
    public void findAllByUserId() throws Exception {

        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(""));
        final List<Project> projects = service.findAll(userId);
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
    }

    @Test
    public void findAllComparatorByUserId() throws Exception {

        @Nullable Comparator comparator = null;
        Assert.assertNotNull(service.findAll(userId, comparator));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Comparator comparatorInner = null;
            service.findAll("", comparatorInner);
        });
        comparator = NameComparator.INSTANCE;
        final List<Project> projects = service.findAll(userId, comparator);
        Assert.assertNotNull(projects);
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
    }

    @Test
    public void findAllSortByUserId() throws Exception {

        @Nullable Sort sort = null;
        Assert.assertNotNull(service.findAll(userId, sort));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Sort sortInner = null;
            service.findAll("", sortInner);
        });
        sort = Sort.BY_NAME;
        final List<Project> projects = service.findAll(userId, sort);
        Assert.assertNotNull(projects);
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
    }

    @Test
    public void findOneByIdByUserId() throws Exception {

        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userId, ""));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById("", USER_PROJECT1.getId()));
        Assert.assertNull(service.findOneById(userId, NON_EXISTING_PROJECT_ID));
        @Nullable final Project project = service.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void getSizeByUserId() throws Exception {

        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(""));
        Assert.assertEquals(2, service.getSize(userId));
    }

    @Test
    public void removeByIdByUserId() throws Exception {

        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeOneById(null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeOneById("", null));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeOneById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeOneById(userId, ""));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.removeOneById(userId, NON_EXISTING_PROJECT_ID));
        service.removeOneById(userId, USER_PROJECT2.getId());
        Assert.assertNull(service.findOneById(userId, USER_PROJECT2.getId()));
    }

    @Test
    public void removeByUserId() throws Exception {

        service.remove(userId, USER_PROJECT2);
        Assert.assertNull(service.findOneById(userId, USER_PROJECT2.getId()));
    }

    @Test
    public void updateById() throws Exception {

        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateOneById(null, USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateOneById("", USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(IdEmptyException.class, () -> service.updateOneById(userId, null, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(IdEmptyException.class, () -> service.updateOneById(userId, "", USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateOneById(userId, USER_PROJECT1.getId(), null, USER_PROJECT1.getDescription()));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateOneById(userId, USER_PROJECT1.getId(), "", USER_PROJECT1.getDescription()));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.updateOneById(userId, USER_PROJECT1.getId(), USER_PROJECT1.getName(), null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.updateOneById(userId, USER_PROJECT1.getId(), USER_PROJECT1.getName(), ""));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.updateOneById(userId, NON_EXISTING_PROJECT_ID, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        @NotNull final String name = USER_PROJECT1.getName() + NON_EXISTING_PROJECT_ID;
        @NotNull final String description = USER_PROJECT1.getDescription() + NON_EXISTING_PROJECT_ID;
        service.updateOneById(userId, USER_PROJECT1.getId(), name, description);
        @Nullable final Project project = service.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

}