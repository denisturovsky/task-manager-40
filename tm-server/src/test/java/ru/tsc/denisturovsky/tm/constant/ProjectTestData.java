package ru.tsc.denisturovsky.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.model.Project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@UtilityClass
public final class ProjectTestData {

    @NotNull
    public final static String NON_EXISTING_PROJECT_ID = UUID.randomUUID().toString();

    @NotNull
    public final static List<Project> PROJECT_LIST = new ArrayList<>();

    @NotNull
    public final static Project USER_PROJECT1 = new Project();

    @NotNull
    public final static Project USER_PROJECT2 = new Project();

    @NotNull
    public final static Project USER_PROJECT3 = new Project();

    @NotNull
    public final static List<Project> USER_PROJECT_LIST = Arrays.asList(USER_PROJECT1, USER_PROJECT2, USER_PROJECT3);

    static {
        USER_PROJECT_LIST.forEach(project -> project.setName("User Test Project " + project.getId()));
        USER_PROJECT_LIST.forEach(project -> project.setDescription("User Test Project " + project.getId() + " description"));
        PROJECT_LIST.add(USER_PROJECT1);
        PROJECT_LIST.add(USER_PROJECT2);
    }

}