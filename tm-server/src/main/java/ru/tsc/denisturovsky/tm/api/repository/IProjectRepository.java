package ru.tsc.denisturovsky.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project (id, name, created, description, user_id, status, date_begin, date_end) " +
            "VALUES (#{project.id}, #{project.name}, #{project.created}, #{project.description}, #{project.userId}, " +
            "#{project.status}, #{project.dateBegin}, #{project.dateEnd})")
    void add(@NotNull @Param("project") Project project);

    @Insert("INSERT INTO tm_project (id, name, created, description, user_id, status, date_begin, date_end) " +
            "VALUES (#{project.id}, #{project.name}, #{project.created}, #{project.description}, #{userId}, " +
            "#{project.status}, #{project.dateBegin}, #{project.dateEnd})")
    void addWithUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("project") Project project
    );

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    @Nullable
    List<Project> findAll(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    @Nullable List<Project> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    @Nullable List<Project> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    @Nullable List<Project> findAllOrderByStatus(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    @Nullable Project findOneById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateBegin", column = "date_begin"),
            @Result(property = "dateEnd", column = "date_end")
    })
    @Nullable Project findOneByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index
    );

    @Select("SELECT COUNT(*) FROM tm_project WHERE user_id = #{userId}")
    int getSize(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_project WHERE user_id = #{project.userId} AND id = #{project.id}")
    void remove(@NotNull @Param("project") Project project);

    @Update("UPDATE tm_project SET name = #{project.name}, created = #{project.created}, " +
            "description = #{project.description}, user_id = #{project.userId}, status = #{project.status}, " +
            "date_begin = #{project.dateBegin}, date_end = #{project.dateEnd} WHERE id = #{project.id}")
    void update(@NotNull @Param("project") Project project);

}