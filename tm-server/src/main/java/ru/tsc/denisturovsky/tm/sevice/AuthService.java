package ru.tsc.denisturovsky.tm.sevice;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.service.IAuthService;
import ru.tsc.denisturovsky.tm.api.service.IPropertyService;
import ru.tsc.denisturovsky.tm.api.service.ISessionService;
import ru.tsc.denisturovsky.tm.api.service.IUserService;
import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.exception.field.LoginEmptyException;
import ru.tsc.denisturovsky.tm.exception.field.PasswordEmptyException;
import ru.tsc.denisturovsky.tm.exception.user.AccessDeniedException;
import ru.tsc.denisturovsky.tm.exception.user.LoginPasswordIncorrectException;
import ru.tsc.denisturovsky.tm.exception.user.UserLockedException;
import ru.tsc.denisturovsky.tm.model.Session;
import ru.tsc.denisturovsky.tm.model.User;
import ru.tsc.denisturovsky.tm.util.CryptUtil;
import ru.tsc.denisturovsky.tm.util.HashUtil;

import java.util.Date;

public class AuthService implements IAuthService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ISessionService sessionService;

    @NotNull
    private final IUserService userService;

    public AuthService(
            @NotNull IPropertyService propertyService,
            @NotNull IUserService userService,
            @NotNull ISessionService sessionService
    ) {

        this.userService = userService;
        this.propertyService = propertyService;
        this.sessionService = sessionService;
    }

    @NotNull
    private Session createSession(@NotNull final User user) throws Exception {

        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        return sessionService.add(session);
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final Session session) {

        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final User user) {

        return getToken(createSession(user));
    }

    @NotNull
    @Override
    public String login(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {

        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new LoginPasswordIncorrectException();
        if (user.getLocked()) throw new UserLockedException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new LoginPasswordIncorrectException();
        if (!hash.equals(user.getPasswordHash())) throw new LoginPasswordIncorrectException();
        return getToken(user);
    }

    @Override
    public void logout(@Nullable Session session) throws Exception {

        if (session == null) throw new AccessDeniedException();
        sessionService.removeOneById(session.getUserId(), session.getId());
    }

    @NotNull
    @Override
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception {

        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    public Session validateToken(@Nullable final String token) throws Exception {

        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull Session session = objectMapper.readValue(json, Session.class);
        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();
        if (!sessionService.existsById(session.getUserId(), session.getId())) throw new AccessDeniedException();
        return session;
    }

}