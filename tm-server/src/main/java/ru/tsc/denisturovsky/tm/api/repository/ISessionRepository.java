package ru.tsc.denisturovsky.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.model.Session;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO tm_session (id, date, user_id, role)" +
            " VALUES (#{session.id}, #{session.date}, #{session.userId}, #{session.role})")
    void add(@NotNull @Param("session") Session session);

    @Insert("INSERT INTO tm_session (id, date, user_id, role)" +
            " VALUES (#{session.id}, #{session.date}, #{userId}, #{session.role})")
    void addWithUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("session") Session session
    );

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_session WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable
    List<Session> findAll(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_session WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable Session findOneById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT * FROM tm_session WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable Session findOneByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index
    );

    @Select("SELECT COUNT(*) FROM tm_session WHERE user_id = #{userId}")
    int getSize(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_session WHERE user_id = #{session.userId} AND id = #{session.id}")
    void remove(@NotNull @Param("session") Session session);

    @Update("UPDATE tm_session SET name = #{session.name}, date = #{session.date}, role = #{session.role} WHERE id = #{session.id}")
    void update(@NotNull @Param("session") Session session);

}