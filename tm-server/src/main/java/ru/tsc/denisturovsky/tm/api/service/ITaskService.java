package ru.tsc.denisturovsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.enumerated.Sort;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface ITaskService {

    @NotNull
    Task add(@NotNull Task model) throws Exception;

    @NotNull
    Task add(
            @Nullable String userId,
            @NotNull Task model
    ) throws Exception;

    void changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    void changeTaskStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    ) throws Exception;

    void clear(@Nullable String userId) throws Exception;

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    ) throws Exception;

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name
    ) throws Exception;

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    boolean existsById(
            @Nullable String userId,
            @Nullable String id
    ) throws Exception;

    @Nullable
    List<Task> findAll(@Nullable String userId) throws Exception;

    @Nullable
    List<Task> findAll(
            @Nullable String userId,
            @Nullable Comparator comparator
    ) throws Exception;

    @Nullable
    List<Task> findAll(
            @Nullable String userId,
            @Nullable Sort sort
    ) throws Exception;

    @Nullable
    List<Task> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    ) throws Exception;

    @Nullable
    Task findOneById(
            @Nullable String userId,
            @Nullable String id
    ) throws Exception;

    @Nullable
    Task findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    ) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    void remove(
            @Nullable String userId,
            @Nullable Task model
    ) throws Exception;

    void removeOneById(
            @Nullable String userId,
            @Nullable String id
    ) throws Exception;

    void removeOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    ) throws Exception;

    void updateOneById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    void updateOneByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    void updateProjectIdById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String projectId
    ) throws Exception;

}
