package ru.tsc.denisturovsky.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.denisturovsky.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.denisturovsky.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.denisturovsky.tm.api.endpoint.IUserEndpoint;
import ru.tsc.denisturovsky.tm.api.service.IPropertyService;
import ru.tsc.denisturovsky.tm.dto.request.*;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.marker.IntegrationCategory;
import ru.tsc.denisturovsky.tm.model.Project;
import ru.tsc.denisturovsky.tm.service.PropertyService;

import java.util.List;

import static ru.tsc.denisturovsky.tm.constant.ProjectTestData.*;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(IntegrationCategory.class)
public final class ProjectEndpointTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpointClient = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IProjectEndpoint projectEndpointClient = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private static final IUserEndpoint userEndpointClient = IUserEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @Nullable
    private static String userToken;

    @Nullable
    private String projectId1;

    @Nullable
    private String projectId2;

    private int projectIndex1;

    private int projectIndex2;

    @BeforeClass
    public static void setUp() {

        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(propertyService.getAdminLogin());
        loginRequest.setPassword(propertyService.getAdminPassword());
        adminToken = authEndpointClient.loginUser(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        request.setPassword(USER_TEST_PASSWORD);
        userEndpointClient.registryUser(request);
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin(USER_TEST_LOGIN);
        userLoginRequest.setPassword(USER_TEST_PASSWORD);
        userToken = authEndpointClient.loginUser(userLoginRequest).getToken();
    }

    @AfterClass
    public static void tearDown() {

        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        userEndpointClient.removeUser(request);
    }

    @After
    public void after() {

        removeTestProjects();
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(userToken);
        Assert.assertNotNull(projectEndpointClient.clearProject(request));
    }

    @Before
    public void before() {

        removeTestProjects();
        projectId1 = createTestProject(USER_PROJECT1_NAME, USER_PROJECT1_DESCRIPTION);
        projectIndex1 = 0;
        projectId2 = createTestProject(USER_PROJECT2_NAME, USER_PROJECT2_DESCRIPTION);
        projectIndex2 = 1;
    }

    @Test
    public void changeStatusByIdProject() {

        @NotNull final Status status = Status.COMPLETED;
        @NotNull final ProjectChangeStatusByIdRequest projectCreateRequestNullToken = new ProjectChangeStatusByIdRequest(null);
        projectCreateRequestNullToken.setId(projectId1);
        projectCreateRequestNullToken.setStatus(status);
        Assert.assertThrows(Exception.class, () -> projectEndpointClient.changeStatusByIdProject(projectCreateRequestNullToken));
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(userToken);
        request.setId(projectId1);
        request.setStatus(status);
        Assert.assertNotNull(projectEndpointClient.changeStatusByIdProject(request));
        @Nullable final Project project = findTestProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(status, project.getStatus());
    }

    @Test
    public void changeStatusByIndexProject() {

        @NotNull final Status status = Status.COMPLETED;
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(userToken);
        request.setIndex(projectIndex1);
        request.setStatus(status);
        Assert.assertNotNull(projectEndpointClient.changeStatusByIndexProject(request));
        @Nullable final Project project = findTestProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(status, project.getStatus());
    }

    @Test
    public void clearProject() {

        @NotNull final ProjectClearRequest request = new ProjectClearRequest(userToken);
        Assert.assertNotNull(projectEndpointClient.clearProject(request));
        @Nullable Project project = findTestProjectById(projectId1);
        Assert.assertNull(project);
        project = findTestProjectById(projectId2);
        Assert.assertNull(project);
    }

    @Test
    public void completeByIdProject() {

        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(userToken);
        request.setId(projectId1);
        Assert.assertNotNull(projectEndpointClient.completeByIdProject(request));
        @Nullable Project project = findTestProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void completeByIndexProject() {

        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(userToken);
        request.setIndex(projectIndex1);
        Assert.assertNotNull(projectEndpointClient.completeByIndexProject(request));
        @Nullable Project project = findTestProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void createProject() {

        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName(USER_PROJECT3_NAME);
        projectCreateRequest.setDescription(USER_PROJECT3_DESCRIPTION);
        @Nullable Project project = projectEndpointClient.createProject(projectCreateRequest).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT3_NAME, project.getName());
        Assert.assertEquals(USER_PROJECT3_DESCRIPTION, project.getDescription());
    }

    @NotNull
    private String createTestProject(
            final String name,
            final String description
    ) {

        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName(name);
        projectCreateRequest.setDescription(description);
        return projectEndpointClient.createProject(projectCreateRequest).getProject().getId();
    }

    @Nullable
    private Project findTestProjectById(final String id) {

        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(userToken);
        request.setId(id);
        return projectEndpointClient.showByIdProject(request).getProject();
    }

    @Test
    public void listProject() {

        @NotNull final ProjectListRequest request = new ProjectListRequest(userToken);
        @Nullable final List<Project> projects = projectEndpointClient.listProject(request).getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
        for (Project project : projects) {
            Assert.assertNotNull(findTestProjectById(project.getId()));
        }
    }

    @Test
    public void removeByIdProject() {

        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(userToken);
        request.setId(projectId2);
        Assert.assertNotNull(projectEndpointClient.removeByIdProject(request));
        Assert.assertNull(findTestProjectById(projectId2));
    }

    @Test
    public void removeByIndexProject() {

        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(userToken);
        request.setIndex(projectIndex2);
        Assert.assertNotNull(projectEndpointClient.removeByIndexProject(request));
        Assert.assertNull(findTestProjectById(projectId2));
    }

    private void removeTestProjects() {

        @NotNull final ProjectListRequest projectListRequest = new ProjectListRequest(userToken);
        @Nullable final List<Project> projects = projectEndpointClient.listProject(projectListRequest).getProjects();
        if (projects != null) {
            for (Project project : projects) {
                @NotNull final ProjectRemoveByIdRequest projectRemoveByIdRequest = new ProjectRemoveByIdRequest(userToken);
                projectRemoveByIdRequest.setId(project.getId());
                projectEndpointClient.removeByIdProject(projectRemoveByIdRequest);
            }
        }
    }

    @Test
    public void showByIdProject() {

        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(userToken);
        request.setId(projectId1);
        @Nullable final Project project = projectEndpointClient.showByIdProject(request).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(projectId1, project.getId());
    }

    @Test
    public void showByIndexProject() {

        @NotNull final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest(userToken);
        request.setIndex(projectIndex1);
        @Nullable final Project project = projectEndpointClient.showByIndexProject(request).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(projectId1, project.getId());
    }

    @Test
    public void startByIdProject() {

        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(userToken);
        request.setId(projectId1);
        Assert.assertNotNull(projectEndpointClient.startByIdProject(request));
        @Nullable Project project = findTestProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    public void startByIndexProject() {

        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(userToken);
        request.setIndex(projectIndex1);
        Assert.assertNotNull(projectEndpointClient.startByIndexProject(request));
        @Nullable Project project = findTestProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    public void updateByIdProject() {

        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(userToken);
        request.setId(projectId1);
        request.setName(USER_PROJECT3_NAME);
        request.setDescription(USER_PROJECT3_DESCRIPTION);
        Assert.assertNotNull(projectEndpointClient.updateByIdProject(request));
        @Nullable Project project = findTestProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT3_NAME, project.getName());
        Assert.assertEquals(USER_PROJECT3_DESCRIPTION, project.getDescription());
    }

    @Test
    public void updateByIndexProject() {

        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(userToken);
        request.setIndex(projectIndex1);
        request.setName(USER_PROJECT3_NAME);
        request.setDescription(USER_PROJECT3_DESCRIPTION);
        Assert.assertNotNull(projectEndpointClient.updateByIndexProject(request));
        @Nullable Project project = findTestProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT3_NAME, project.getName());
        Assert.assertEquals(USER_PROJECT3_DESCRIPTION, project.getDescription());
    }

}