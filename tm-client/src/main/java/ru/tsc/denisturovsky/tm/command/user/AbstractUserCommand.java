package ru.tsc.denisturovsky.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.denisturovsky.tm.api.endpoint.IUserEndpoint;
import ru.tsc.denisturovsky.tm.command.AbstractCommand;
import ru.tsc.denisturovsky.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @Nullable
    @Override
    public String getArgument() {

        return null;
    }

    @NotNull
    protected IAuthEndpoint getAuthEndpoint() {

        assert serviceLocator != null;
        return serviceLocator.getAuthEndpointClient();
    }

    @NotNull
    protected IUserEndpoint getUserEndpoint() {

        assert serviceLocator != null;
        return serviceLocator.getUserEndpointClient();
    }

    protected void showUser(@Nullable final User user) {

        if (user == null) return;
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
