package ru.tsc.denisturovsky.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Locale;

public final class CommandListCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-cmd";

    @NotNull
    public static final String DESCRIPTION = "Show command list";

    @NotNull
    public static final String NAME = "commands";

    @Override
    public void execute() {

        System.out.format("[%s] \n", NAME.toUpperCase(Locale.ROOT));
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        commands.forEach(m -> {
            @NotNull final String name = m.getName();
            if (!name.isEmpty()) System.out.println(name);
        });
    }

    @NotNull
    @Override
    public String getArgument() {

        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {

        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {

        return NAME;
    }

}
