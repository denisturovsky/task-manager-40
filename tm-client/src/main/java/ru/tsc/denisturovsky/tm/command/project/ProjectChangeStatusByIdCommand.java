package ru.tsc.denisturovsky.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.dto.request.ProjectChangeStatusByIdRequest;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Change project status by id";

    @NotNull
    public static final String NAME = "project-change-status-by-id";

    @Override
    public void execute() {

        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken());
        request.setId(id);
        request.setStatus(status);
        getProjectEndpoint().changeStatusByIdProject(request);
    }

    @NotNull
    @Override
    public String getDescription() {

        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {

        return NAME;
    }

}
