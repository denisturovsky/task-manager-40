package ru.tsc.denisturovsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.dto.request.TaskShowByIdRequest;
import ru.tsc.denisturovsky.tm.model.Task;
import ru.tsc.denisturovsky.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Show task by id";

    @NotNull
    public static final String NAME = "task-show-by-id";

    @Override
    public void execute() {

        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(getToken());
        request.setId(id);
        @Nullable final Task task = getTaskEndpoint().showByIdTask(request).getTask();
        showTask(task);
    }

    @NotNull
    @Override
    public String getDescription() {

        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {

        return NAME;
    }

}
