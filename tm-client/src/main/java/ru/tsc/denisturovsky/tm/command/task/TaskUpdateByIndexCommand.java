package ru.tsc.denisturovsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.dto.request.TaskUpdateByIndexRequest;
import ru.tsc.denisturovsky.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Update task by index";

    @NotNull
    public static final String NAME = "task-update-by-index";

    @Override
    public void execute() {

        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(getToken());
        request.setIndex(index);
        request.setName(name);
        request.setDescription(description);
        getTaskEndpoint().updateByIndexTask(request);
    }

    @NotNull
    @Override
    public String getDescription() {

        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {

        return NAME;
    }

}
