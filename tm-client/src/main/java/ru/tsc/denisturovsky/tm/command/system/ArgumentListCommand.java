package ru.tsc.denisturovsky.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Locale;

public final class ArgumentListCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    public static final String DESCRIPTION = "Show argument list";

    @NotNull
    public static final String NAME = "arguments";

    @Override
    public void execute() {

        System.out.format("[%s] \n", NAME.toUpperCase(Locale.ROOT));
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        commands.forEach(m -> {
            @Nullable final String argument = m.getArgument();
            if (argument != null && !argument.isEmpty()) System.out.println(argument);
        });
    }

    @NotNull
    @Override
    public String getArgument() {

        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {

        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {

        return NAME;
    }

}
