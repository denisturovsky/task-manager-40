package ru.tsc.denisturovsky.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.api.service.IPropertyService;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String ADMIN_LOGIN_KEY = "admin.login";

    @NotNull
    private static final String ADMIN_PASSWORD_KEY = "admin.password";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "127.0.0.1";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "6060";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {

        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    @Override
    public String getAdminLogin() {

        return getStringValue(ADMIN_LOGIN_KEY, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getAdminPassword() {

        return getStringValue(ADMIN_PASSWORD_KEY, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {

        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {

        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    @Override
    public String getHost() {

        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getPort() {

        return getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @NotNull
    private String getStringValue(
            @NotNull final String key,
            @NotNull final String defaultValue
    ) {

        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getProperties().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

}
