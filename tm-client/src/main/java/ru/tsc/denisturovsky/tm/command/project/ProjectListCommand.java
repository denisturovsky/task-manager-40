package ru.tsc.denisturovsky.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.dto.request.ProjectListRequest;
import ru.tsc.denisturovsky.tm.enumerated.Sort;
import ru.tsc.denisturovsky.tm.model.Project;
import ru.tsc.denisturovsky.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Show project list";

    @NotNull
    public static final String NAME = "project-list";

    @Override
    public void execute() {

        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken());
        request.setSort(sort);
        @Nullable final List<Project> projects = getProjectEndpoint().listProject(request).getProjects();
        if (projects == null) return;
        System.out.printf("|%-30s:%30s:%30s:%30s:%30s:%30s|%n",
                "INDEX",
                "NAME",
                "STATUS",
                "DESCRIPTION",
                "DATE BEGIN",
                "DATE END"
        );
        @NotNull final int[] index = {1};
        projects.forEach(m -> {
            System.out.printf("|%-30s:%30s%n", index[0], m);
            index[0]++;
        });
    }

    @NotNull
    @Override
    public String getDescription() {

        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {

        return NAME;
    }

}
