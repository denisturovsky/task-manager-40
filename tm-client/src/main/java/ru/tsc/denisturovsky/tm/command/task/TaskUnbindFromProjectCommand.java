package ru.tsc.denisturovsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.tsc.denisturovsky.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Unbind task from project";

    @NotNull
    public static final String NAME = "task-unbind-from-project";

    @Override
    public void execute() {

        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(getToken());
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        getTaskEndpoint().unbindFromProjectTask(request);
    }

    @NotNull
    @Override
    public String getDescription() {

        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {

        return NAME;
    }

}
