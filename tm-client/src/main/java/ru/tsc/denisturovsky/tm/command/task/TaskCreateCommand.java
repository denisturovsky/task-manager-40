package ru.tsc.denisturovsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.dto.request.TaskCreateRequest;
import ru.tsc.denisturovsky.tm.util.TerminalUtil;

import java.util.Date;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Create new task";

    @NotNull
    public static final String NAME = "task-create";

    @Override
    public void execute() {

        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        System.out.println("ENTER DATE BEGIN:");
        @NotNull final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("ENTER DATE END:");
        @NotNull final Date dateEnd = TerminalUtil.nextDate();
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(getToken());
        request.setName(name);
        request.setDescription(description);
        request.setDateBegin(dateBegin);
        request.setDateEnd(dateEnd);
        getTaskEndpoint().createTask(request);
    }

    @NotNull
    @Override
    public String getDescription() {

        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {

        return NAME;
    }

}
