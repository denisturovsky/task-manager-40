package ru.tsc.denisturovsky.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.dto.request.ApplicationAboutRequest;
import ru.tsc.denisturovsky.tm.dto.response.ApplicationAboutResponse;

import java.util.Locale;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "Show developer info";

    @NotNull
    public static final String NAME = "about";

    @Override
    public void execute() {

        @NotNull final ApplicationAboutRequest request = new ApplicationAboutRequest();
        @NotNull final ApplicationAboutResponse response = getSystemEndpoint().getApplicationAbout(request);
        System.out.format("[%s] \n", NAME.toUpperCase(Locale.ROOT));
        System.out.format("Name: %s \n", response.getName());
        System.out.format("E-mail: %s \n", response.getEmail());
    }

    @NotNull
    @Override
    public String getArgument() {

        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {

        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {

        return NAME;
    }

}
