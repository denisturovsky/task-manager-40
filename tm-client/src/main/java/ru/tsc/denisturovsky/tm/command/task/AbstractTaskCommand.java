package ru.tsc.denisturovsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.denisturovsky.tm.command.AbstractCommand;
import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.model.Task;
import ru.tsc.denisturovsky.tm.util.DateUtil;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @Nullable
    @Override
    public String getArgument() {

        return null;
    }

    @Nullable
    public Role[] getRoles() {

        return Role.values();
    }

    protected ITaskEndpoint getTaskEndpoint() {

        assert serviceLocator != null;
        return serviceLocator.getTaskEndpointClient();
    }

    protected void renderTasks(@NotNull final List<Task> tasks) {

        System.out.printf("|%-30s:%30s:%30s:%30s:%30s:%30s|%n",
                "INDEX",
                "NAME",
                "STATUS",
                "DESCRIPTION",
                "DATE BEGIN",
                "DATE END"
        );
        final int @NotNull [] index = {1};
        tasks.forEach(m -> {
            System.out.printf("|%-30s:%30s%n", index[0], m);
            index[0]++;
        });
    }

    protected void showTask(@Nullable final Task task) {

        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(task.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(task.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(task.getDateEnd()));
    }

}
