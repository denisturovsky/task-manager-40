package ru.tsc.denisturovsky.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.dto.request.UserViewProfileRequest;
import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.model.User;

public final class UserShowProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Show user info";

    @NotNull
    public static final String NAME = "user-show-profile";

    @Override
    public void execute() {

        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest(getToken());
        @Nullable final User user = getAuthEndpoint().viewProfileUser(request).getUser();
        showUser(user);
    }

    @NotNull
    @Override
    public String getDescription() {

        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {

        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {

        return Role.values();
    }

}
